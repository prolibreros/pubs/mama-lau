# 5

Hola, yo soy Lilik y ella es mi mamá Lau.

# 6

Aunque casi nunca nos separamos,
hace días que mi abuelita no llega a casa.

# 7

¿Dónde podrá estar?

# 8

Mi tía Paulina dice que mamá Lau
está allá arriba, entre las nubes.

# 9

Pero mi abuelita es muy gordita,
no podría flotar.

# 10

Don Zorrillo piensa que ya se quedó
a bailar con la calaca.

# 11

¡Qué loco! Los huesos no se mueven,
¿cómo podrían zapatear?

# 12

_Miss_ Andrea me cuenta que el más viejo
entre los viejos vino por ella.

# 13

No le creo. Mamá Lau dice que los dones
son unos latosos, que prefiere a Soledad.

# 14

El señor que vive en la esquina cree
que mi abuelita cayó en el olvido.

# 15

Pero nosotros aún la recordamos,
jamás podríamos dejarla ir en paz.

# 16

Mayra, la amiga de mi mamá, me contó que
nuestra última parada es en Ningún Lugar.

# 17

No la entiendo: un lugar siempre es algún lugar.

# 18

«¿Y si todo es un sueño?»,
me pregunta mi tío Ramiro.

# 19

¿Qué caso tiene? No encuentro a mi mamá Lau,
esté o no teniendo una pesadilla...

# 20

¡Quiero a mi abuelita! ¡Extraño a mamá Lau!

# 21

Mi mami viene a reconfortarme:

---Mamá Lau podría estar en cualquier lugar.
---Pero ¡yo quiero saber dónde está mi Mamá Lau!
---¿Acaso saber a dónde vamos te quitaría el dolor?
---Quizá me aliviaría un poco.
---Entonces, ¿por qué no creer que Mamá Lau…

# 22

---…se convirtió en sirena?

# 23

---¿Que tal si está volando con Cupido?

# 24

---¿O enterrada con los gusanos?

# 25

---¡O recorriendo el mundo con los fantasmas!

# 26

---¿Podría ser una paloma de tres cabezas?

# 27

---¡O una abuela sin cabeza! ¡Bu!

# 28

---Entonces, ¿por qué buscamos?

# 29

Mi mami suspira profundamente.

# 30

---Las personas buscamos porque así
mantenemos viva la memoria de los que ya no están.
